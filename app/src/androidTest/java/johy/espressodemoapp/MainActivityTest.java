package johy.espressodemoapp;

import android.support.design.widget.Snackbar;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.EditText;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isChecked;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withHint;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.*;

/**
 * Created by Johy on 31. 7. 2018.
 */
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void typeHelloAndClick() {
        onView(withId(R.id.editText))
                .check(matches(withHint("type text")))
                .perform(typeText("Hello world"))
                .check(matches(withText("Hello world")));
        onView(withId(R.id.button))
                .perform(click());
        onView(withId(R.id.textview))
                .check(matches(withText("Text: Hello world")));
        onView(withId(R.id.checkbox))
                .check(matches(isChecked()));
    }

    @Test
    public void showSnackBar() {
        onView(withId(R.id.fab))
                .perform(click());
        onView(withText("Replace with your own action"))
                .check(matches(isDisplayed()));
    }

    @Test
    public void typeNumber() {
        onView(allOf(instanceOf(EditText.class), withHint("Type number")))
                .check(matches(withText("")))
                .perform(typeText("123456"))
                .check(matches(withText("123456")))
                .perform(typeText("abc"))
                .check(matches(not(withText("abc"))))
                .check(matches(withText(startsWith("123"))));
    }
}