package johy.espressodemoapp;

import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withHint;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.*;

/**
 * Created by Johy on 4. 8. 2018.
 */
public class ListViewActivityTest {

    @Rule
    public ActivityTestRule<ListViewActivity> activityTestRule = new ActivityTestRule<>(ListViewActivity.class);

    @Test
    public void clickOnTheRow() {
        onView(withId(R.id.listview))
                .check(matches(isDisplayed()));
        ViewInteraction row = onData(anything()).inAdapterView(withId(R.id.listview))
                .atPosition(0)
                .check(matches(withText("Apple")))
                .perform(click());
        onView(withText("Cancel"))
                .perform(click());
        row.check(matches(withText("Apple")))
                .perform(click());
        onView(withText("Delete"))
                .perform(click());
        row.check(matches(withText(not("Apple"))))
                .check(matches(withText("Strawberry")));
    }

    @Test
    public void finishing() {
        pressBack();
        assertTrue(activityTestRule.getActivity().isFinishing());
    }

}