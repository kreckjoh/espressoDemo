package johy.espressodemoapp;

import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiSelector;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.longClick;

import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by Johy on 5. 8. 2018.
 */
public class RecyclerViewActivityTest {

    private UiDevice mDevice;

    @Rule
    public ActivityTestRule<RecyclerViewActivity> activityTestRule = new ActivityTestRule<>(RecyclerViewActivity.class);

    @Test
    public void clickonRow() {
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        onView(withId(R.id.recycler_view))
                .perform(RecyclerViewActions.actionOnItemAtPosition(3, click()));
        UiObject toast = mDevice.findObject(new UiSelector()
                .text("Row clicked"));
        Assert.assertTrue(toast.exists());
        pressBack();
        onView(withId(R.id.recycler_view))
                .perform(RecyclerViewActions.actionOnItemAtPosition(3, longClick()));
        toast = mDevice.findObject(new UiSelector()
                .text("Row long clicked"));
        Assert.assertTrue(toast.exists());
    }
}